#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>


using namespace std;
//std::stringstream s;

unordered_map<string, vector<string>> results;
int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	return 0;
}


int main()
{ 
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;

	// connection to the database
	rc = sqlite3_open("FirstPart.db", &db);

	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}

	system("CLS");
	cout << "create table people(id integer primary key autoincrement, name string)" << endl;

	rc = sqlite3_exec(db, "create table people(id integer primary key autoincrement, name string);", callback, NULL, &zErrMsg);

	if(rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
	}
	else
	{
		cout << "table created successfully\n" << endl; ////
	}

	system("pause");
	system("CLS");

	cout << "insert into people (name) values('Dinno')" << endl;
	if (flag)
	{
		rc = sqlite3_exec(db, "insert into people(name) values('Dinno')", NULL, 0, &zErrMsg);

		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}
	}
//	last_insert_rowid();
	system("pause");
	system("CLS");


	cout << "insert into people (name) values('alona')" << endl;
	if (flag)
	{
		rc = sqlite3_exec(db, "insert into people(name) values('alona')", NULL, 0, &zErrMsg);

		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}
	}
	//	last_insert_rowid();
	system("pause");
	system("CLS");


	cout << "insert into people (name) values('jewle')" << endl;
	if (flag)
	{
		rc = sqlite3_exec(db, "insert into people(name) values('jewle')", NULL, 0, &zErrMsg);

		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}
	}
	//	last_insert_rowid();
	system("pause");
	system("CLS");


	//changing the last name
	cout << "insert into people (id, name) values(last_insert_rowid(), 'july');" << endl;
	if (flag)
	{
		rc = sqlite3_exec(db, "insert into people(id, name) values(last_insert_rowid(), 'july');", NULL, 0, &zErrMsg);

		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}
	}
	//	last_insert_rowid();

	system("PAUSE");
	//system("CLS");

}